#!/usr/bin/env python3
# python script picoloadfst.py --- creates pkb containing finite state trasducer.
#
# Written from scratch based on SVOX specs and comparing output of picoloadfst.exe
#
# Copyright 2021 Jeremy Whiting <jpwhiting@kde.org>
#
# load pico fst src file and create pkb file for the knowledge base
#
# accepted syntax:
#   See chapter 2 of SVOX_Pico_Lingware.pdf

import argparse
import os
import re
import struct

alphabet = []
classifications = {}
defaultclass = 1 # until told otherwise
transitions = {}

def loadAlphabet(infile, line):
    stringRE = re.compile('(\\d+)\\s+"(.*)"', re.UNICODE)
    otherStringRE = re.compile("(\\d+)\\s+'(.*)'", re.UNICODE)
    line = infile.readline()
    while line != '*\n':
        m = stringRE.match(line)
        if m == None:
            m = otherStringRE.match(line)
            if m == None:
                print("*** error: string line didn't match syntax: {}".format(line))
                exit(1)

        # Read offset
        offset = int(m.group(1))

        if offset != currentStringsLength:
            print("*** error: strings line {} offset doesn't match current offset {}".format(line, currentStringsLength))
            exit(1)

        # read string
        string = m.group(2)
        strings.append(string)
        currentStringsLength += len(string.encode()) + 1 # +1 for terminating 0 character

        line = infile.readline()

# Classes are one line each, just load the details from the rest of the line
def loadClass(infile, line):
    stringRE = re.compile(':CLASS\\s+(\\d+)\\s+:IS.*', re.UNICODE)
    m = stringRE.match(line)
    if (m == None):
        print("*** erorr :CLASS line invalid format {}".format(line))
        exit(1)

    classifications[m.group(1)] = ''

def loadDefaultClass(infile, line):
    stringRE = re.compile(':DEFAULTCLASS\\s+(\\d+).*', re.UNICODE)
    m = stringRE.match(line)
    if (m == None):
        print("*** erorr :CLASS line invalid format {}".format(line))
        exit(1)

    defaultclass = m.group(1)
    classifications[defaultclass] = ''


def loadTransition(infile, line):
    stringRE = re.compile(':TRANS\\s+(\\d+)\\s+:TO\\s+(.*)', re.UNICODE)
    m = stringRE.match(line)
    if (m == None):
        print("*** error: transformation line invalid format {}".format(line))
        exit(1)

    # TODO: Read transition array
    transitions[m.group(1)] = []


def makeRE(name, loaderfunction):
    expression = re.compile('{}\\s+(\\d+)'.format(name))
    return { "name": name, "expression": expression, "length": 0, "loader": loaderfunction}

# Comment regular expression
commentLine = re.compile('^\\s*!.*$')

# Make regular expressions for each type of section
matchers = [ makeRE(":ALPHABET", loadAlphabet),
          makeRE(":CLASS", loadClass),
          makeRE(":TRANS", loadTransition),
          makeRE(":AUTOMATON", None),
          makeRE(":DEFAULTCLASS", loadDefaultClass)
 ]

args = argparse.Namespace()

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('infile', type=argparse.FileType('r', encoding='UTF-8'),
                    help='source file name of text preprocessing network data')
parser.add_argument('symboflile', type=argparse.FileType('r', encoding='UTF-8'),
                    help='symbol file for alphabet symbols')
parser.add_argument('outfile', type=argparse.FileType('wb'),
                    help='destination file name of pkb data')

parser.parse_args(namespace=args)

if not args.infile:
    print("*** error: could not open input file: " + args.infile)
    exit(1)

if not args.outfile:
    print("*** error: could not open output file: " + args.outfile)
    exit(1)

# Pico does some weird set msb for positive numbers for some reason...
def valBytes(value, bytes):
    return value + (1 << ((bytes * 8) - 1))

def signedValue(value):
    bytes = 0
    temp = value * 2 # (last bit is 1 for negative)
    if (temp >= 128):
        bytes = 0x0100 | temp
    else: # Less than 128, so set high bit to trigger complete
        bytes = 0x0080 | temp
    return bytes


# Parse file getting each section as we come to it, including setting sizes for headers
# Also sanity check input in case it has errors, size overflows, etc.
line = args.infile.readline()
while line:
    #    -- discard comment-only lines$
    if commentLine.match(line):
        line = args.infile.readline()
        continue

    for item in matchers:
        if item["expression"].match(line):
            if item.get("loader"):
                # Run loader for this type
                item.get("loader")(args.infile, line)

    line = args.infile.readline()

# Write the pkb file header (4 bytes of 'x')
for i in range(0, 4):
    args.outfile.write(struct.pack('B', ord('x')))

# Next write transduction mode in weird signed number format
# All transduction mode in pico is ignored, but examples are
# 0 anyway, so just write an 0x80 here for now
args.outfile.write(struct.pack('B', 0x80))

# Next write the number of classes
classesBytes = signedValue(len(classifications))
if (classesBytes <= 0xFF):
    args.outfile.write(struct.pack('B', classesBytes))
elif (classesBytes <= 0xFFFF):
    args.outfile.write(struct.pack('>H', classesBytes))
else:
    print("*** error: Value {} has exceeded 2 bytes variable rate number".format(len(classesBytes)))

# Next write out the number of states (number of transitions)
print("number of transitions: {}".format(len(transitions)))
transBytes = signedValue(len(transitions))
if (transBytes <= 0xFF):
    args.outfile.write(struct.pack('B', transBytes))
elif (transBytes <= 0xFFFF):
    args.outfile.write(struct.pack('>H', transBytes)) # Use big endian since we already did the or in signedValue

args.outfile.close()
